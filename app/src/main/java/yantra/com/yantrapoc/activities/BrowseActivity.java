package yantra.com.yantrapoc.activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import yantra.com.yantrapoc.R;
import yantra.com.yantrapoc.adapters.FilesListAdapter;
import yantra.com.yantrapoc.database.YantraSqliteDatabase;
import yantra.com.yantrapoc.models.DependenciesModel;
import yantra.com.yantrapoc.shared.CommonUi;

public class BrowseActivity extends AppCompatActivity {

    RelativeLayout videosCont,imagesContent,htmlsContent;
    ListView videosListView,imagesListView,htmlsListView;
    TextView emptyVideoTextView,emptyImagesTextView,emptyHtmlTextView;
    Button videoBtn,imagesBtn,htmlBtn;
    private YantraSqliteDatabase yantraSqliteDatabase;
    ArrayList<DependenciesModel> allDependenciesModelsList;
    ArrayList<DependenciesModel> videoDependenciesModelsList;
    ArrayList<DependenciesModel> imageDependenciesModelsList;
    private ArrayList<DependenciesModel> htmlDependenciesList;
    CommonUi commonUi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        commonUi=new CommonUi(BrowseActivity.this);
        setContentView(R.layout.activity_browse);
        videoBtn =(Button) findViewById(R.id.btn_showVideos);
        imagesBtn =(Button) findViewById(R.id.btn_showImages);
        htmlBtn =(Button) findViewById(R.id.btn_showHmlUrls);
        videosCont = (RelativeLayout) findViewById(R.id.layout_videosContent);
        imagesContent= (RelativeLayout) findViewById(R.id.layout_imagesContent);
        htmlsContent= (RelativeLayout) findViewById(R.id.layout_htmlsContent);
        videosListView =(ListView) findViewById(R.id.lv_videoslist);
        imagesListView=(ListView) findViewById(R.id.lv_imageslist);
        htmlsListView=(ListView) findViewById(R.id.lv_htmlslist);
        emptyVideoTextView = (TextView) findViewById(R.id.tv_emptyvideos);
        emptyImagesTextView= (TextView) findViewById(R.id.tv_emptyimages);
        emptyHtmlTextView= (TextView) findViewById(R.id.tv_emptyhtmls);

        yantraSqliteDatabase=new YantraSqliteDatabase(BrowseActivity.this);
        allDependenciesModelsList=yantraSqliteDatabase.getDependencies();
        videoDependenciesModelsList=new ArrayList<>();
        imageDependenciesModelsList=new ArrayList<>();
        htmlDependenciesList=new ArrayList<>();
        for(int i=0;i<allDependenciesModelsList.size();i++){
            if(allDependenciesModelsList.get(i).getType().equals("VIDEO"))
            videoDependenciesModelsList.add(allDependenciesModelsList.get(i));
            else if(allDependenciesModelsList.get(i).getType().equals("IMAGE"))
            imageDependenciesModelsList.add(allDependenciesModelsList.get(i));
            else if(allDependenciesModelsList.get(i).getType().equals("HTML"))
            htmlDependenciesList.add(allDependenciesModelsList.get(i));
        }

        imagesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showimages();
            }
        });


        videoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showVideos();
            }
        });

        htmlBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showHtmls();
            }
        });
        findViewById(R.id.tv_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navToHomeAct();
            }
        });

        showVideos();

    }
    public void navToHomeAct(){
        Intent homeActIntent=new Intent(BrowseActivity.this,MainActivity.class);
        startActivity(homeActIntent);
        finish();
    }

    public void showVideos(){
       new AsyncTask<Void,Void,Void>(){
           @Override
           protected void onPreExecute() {
               super.onPreExecute();
                commonUi.showLoading("Loading");

           }

           @Override
           protected Void doInBackground(Void... voids) {
               runOnUiThread(new Runnable() {
                   @Override
                   public void run() {
                       videosCont.setVisibility(View.VISIBLE);
                       imagesContent.setVisibility(View.GONE);
                       htmlsContent.setVisibility(View.GONE);
                       if(videoDependenciesModelsList.size()>0){
                           FilesListAdapter filesListAdapter=new FilesListAdapter(videoDependenciesModelsList,getApplicationContext());
                           videosListView.setAdapter(filesListAdapter);
                           videosListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                               @Override
                               public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                                   try {
//                        MediaPlayer mp = new MediaPlayer();
//                        mp.setDataSource(videoDependenciesModelsList.get(position).getLocalPath());
//                        mp.prepare();
//                        mp.start();
                                       Intent videoIntent = new Intent(Intent.ACTION_VIEW);
                                       videoIntent.setDataAndType(Uri.parse(videoDependenciesModelsList.get(position).getLocalPath()), "video/*");
                                       startActivity(videoIntent);
                                   }
                                   catch (Exception e){
                                       Log.e("VideoList","onitemclick exception  = "+e.toString());
                                   }
                               }
                           });
                           videosListView.setVisibility(View.VISIBLE);
                           emptyVideoTextView.setVisibility(View.GONE);

                       }
                       else{

                           videosListView.setVisibility(View.GONE);
                           emptyVideoTextView.setVisibility(View.VISIBLE);
                       }
                   }
               });
               return null;
           }

           @Override
           protected void onPostExecute(Void aVoid) {
               super.onPostExecute(aVoid);
               new Handler().postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       commonUi.hideLoading();
                   }
               },1000);

           }
       }.execute();


    }
    public void showimages(){
        commonUi.showLoading("Loading");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                imagesContent.setVisibility(View.VISIBLE);
                videosCont.setVisibility(View.GONE);
                htmlsContent.setVisibility(View.GONE);
                if(imageDependenciesModelsList.size()>0){
                    FilesListAdapter filesListAdapter=new FilesListAdapter(imageDependenciesModelsList,getApplicationContext());
                    imagesListView.setAdapter(filesListAdapter);

                    imagesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setDataAndType(Uri.parse(imageDependenciesModelsList.get(position).getCdnPath()), "image/*");
                            startActivity(i);
                        }
                    });
                    imagesListView.setVisibility(View.VISIBLE);
                    emptyImagesTextView.setVisibility(View.GONE);
                }
                else{
                    imagesListView.setVisibility(View.GONE);
                    emptyImagesTextView.setVisibility(View.VISIBLE);
                }
                commonUi.hideLoading();
            }
        },100);

    }

    public void showHtmls(){
        commonUi.showLoading("Loading...");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                htmlsContent.setVisibility(View.VISIBLE);
                videosCont.setVisibility(View.GONE);
                imagesContent.setVisibility(View.GONE);
                if(htmlDependenciesList.size()>0){
                    FilesListAdapter filesListAdapter=new FilesListAdapter(htmlDependenciesList,getApplicationContext());
                    htmlsListView.setAdapter(filesListAdapter);
                    htmlsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(htmlDependenciesList.get(position).getCdnPath()));
                            startActivity(browserIntent);
                        }
                    });
                    htmlsListView.setVisibility(View.VISIBLE);
                    emptyHtmlTextView.setVisibility(View.GONE);
                }
                else{
                    htmlsListView.setVisibility(View.GONE);
                    emptyHtmlTextView.setVisibility(View.VISIBLE);
                }
                commonUi.hideLoading();
            }
        },100);
    }
}
