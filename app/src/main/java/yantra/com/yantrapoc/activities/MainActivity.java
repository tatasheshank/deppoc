package yantra.com.yantrapoc.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import yantra.com.yantrapoc.R;
import yantra.com.yantrapoc.receivers.DesReceiver;
import yantra.com.yantrapoc.services.FileMangerService;
import yantra.com.yantrapoc.shared.CommonUi;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button startServiceBtn,stopServiceBtn;
    private Intent fileManServiceIntent;
    SharedPreferences sharedPreference;
    private CommonUi commonUi;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private SharedPreferences.Editor editor;
    private Button browseActBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startServiceBtn =(Button) findViewById(R.id.btn_startService);
        stopServiceBtn =(Button) findViewById(R.id.btn_stopService);
        browseActBtn= (Button) findViewById(R.id.tv_openFiles);
        startServiceBtn.setOnClickListener(this);
        stopServiceBtn.setOnClickListener(this);
        browseActBtn.setOnClickListener(this);
        sharedPreference=getSharedPreferences("ServiceSp",MODE_PRIVATE);


        commonUi=new CommonUi(MainActivity.this);

        createFoldersForDependencies();
        fileManServiceIntent = new Intent(MainActivity.this, FileMangerService.class);

        checkAndRequsetStoragePer();

//        commonUi.enable1autoStart();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        stopService(fileManServiceIntent);
//        startFileManService();
    }

    @Override
    public void onClick(View view) {


        if(view.getId() == R.id.btn_startService){

            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Request For AutoStart")
                    .setMessage(String.format("Please find 'YantraPoc' in autostart and enable it for background progress. ", getString(R.string.app_name)))
                    .setPositiveButton("Protected Apps", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            commonUi.enable1autoStart();
                            dialog.dismiss();
                            checkAndRequsetStoragePer();
                        }
                    })
                    .setNegativeButton("Ignore", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            checkAndRequsetStoragePer();
                        }
                    })
                    .show();
//            Toast.makeText(getApplicationContext(),"Please find 'YantraPoc' in autostart and enable it for background progress.",Toast.LENGTH_LONG).show();


        }
        if(view.getId() == R.id.btn_stopService){
            if(editor!=null) {
                commonUi.showToast("Background service stopped");

                editor = sharedPreference.edit();
                editor.putBoolean("isStartClicked", false);
                editor.apply();
                stopService(fileManServiceIntent);

                editor.clear();

            }
        }

        if(view.getId() == R.id.tv_openFiles){
            navToBrowseAct();
        }


    }

    public void checkAndRequsetStoragePer(){
        if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            //Got Permission
            startFileManService();
        }
        else{
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);

        }
    }
    public void startFileManService(){
        editor=sharedPreference.edit();
        editor.putBoolean("isStartClicked",true);
        editor.apply();
        editor.clear();
        startService(fileManServiceIntent);
        commonUi.showToast("Background service started");
    }

    public void createFoldersForDependencies(){
        commonUi.createDirInStorage("YantraDepends");
        commonUi.createDirInStorage("YantraDepends/Images");
        commonUi.createDirInStorage("YantraDepends/Videos");
        commonUi.createDirInStorage("YantraDepends/Audios");
        commonUi.createDirInStorage("YantraDepends/HTML");

    }

    public void navToBrowseAct(){
        commonUi.showLoading("Loading");
        Intent browseActIntent=new Intent(MainActivity.this,BrowseActivity.class);
        startActivity(browseActIntent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                startFileManService();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setCancelable(false);
                    builder.setTitle("Permission Request");
                    builder.setMessage("Please grant 'STORAGE' permission to continue");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                            dialog.cancel();

                        }
                    });

                    builder.show();                }
            }
        }
    }


}
