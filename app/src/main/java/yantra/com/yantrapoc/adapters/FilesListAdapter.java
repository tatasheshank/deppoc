package yantra.com.yantrapoc.adapters;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import yantra.com.yantrapoc.R;
import yantra.com.yantrapoc.models.DependenciesModel;
import yantra.com.yantrapoc.shared.CommonUi;

/**
 * Created by doveloper system4 on 11/06/2018.
 */

public class FilesListAdapter extends ArrayAdapter<DependenciesModel>{

    private ArrayList<DependenciesModel> dependenciesModelList;
    Context mContext;
    private static final long  MEGABYTE = 1024L * 1024L;

    // View lookup cache
    private static class ViewHolder {
        TextView fileName;
        TextView fileSize;
        ImageView fileThumbView;
    }

    public FilesListAdapter(ArrayList<DependenciesModel> dependenciesModelList, Context context) {
        super(context, R.layout.filelistitem, dependenciesModelList);
        this.dependenciesModelList= dependenciesModelList;
        this.mContext=context;

    }



    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        DependenciesModel dependenciesModel= dependenciesModelList.get(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.filelistitem, parent, false);
            viewHolder.fileName= (TextView) convertView.findViewById(R.id.tv_fileName);
            viewHolder.fileSize = (TextView) convertView.findViewById(R.id.tv_filesize);
            viewHolder.fileThumbView = (ImageView) convertView.findViewById(R.id.iv_fileItemThumbNail);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

//        AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(mContext, (position > lastPosition) ? R.animator.up_from_bottom : R.animator.down_from_top);
//        set.setTarget(convertView); // set the view you want to animate
//        set.start();
        lastPosition = position;
        File file=new File(dependenciesModel.getLocalPath());
        double fileLength= CommonUi.roundDoubleValue((double) file.length()/(1024*1024),2);
        viewHolder.fileName.setText(dependenciesModel.getName());
        viewHolder.fileSize.setText(fileLength+ " Mb");
        ImageView playIcon=(ImageView) convertView.findViewById(R.id.playicon);
        if(dependenciesModel.getType().equals("VIDEO")){
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(dependenciesModel.getLocalPath(), MediaStore.Video.Thumbnails.MINI_KIND);
            viewHolder.fileThumbView.setImageBitmap(thumb);
            playIcon.setVisibility(View.VISIBLE);
        }else if(dependenciesModel.getType().equals("IMAGE")){
            File imgFile = new File(dependenciesModel.getLocalPath());
            playIcon.setVisibility(View.GONE);

            if (imgFile.exists()) {
                Drawable d = Drawable.createFromPath(imgFile.getAbsolutePath());
                viewHolder.fileThumbView.setImageDrawable(d);
            }
//                Bitmap imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//            viewHolder.fileThumbView.setImageBitmap(imgBitmap);
        }
        // Return the completed view to render on screen
        return result;
    }
    public static long bytesToMeg(long bytes) {
        return bytes / MEGABYTE ;
    }
}
