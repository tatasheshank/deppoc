package yantra.com.yantrapoc.shared;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import yantra.com.yantrapoc.R;

/**
 * Created by mypc on 6/10/2018.
 */

public class ApiHandler {
    public Context currentAct;
    private RequestQueue mRequestQueue;
    public JSONObject responseJson;
    public ApiHandler(Context currentAct) {
        this.currentAct = currentAct;


    }

    public void fetchFiles(final Runnable postSuccRunnable){
        String apiUrl=currentAct.getResources().getString(R.string.api_url);
        mRequestQueue = Volley.newRequestQueue(currentAct.getApplicationContext());
        JsonObjectRequest jsonRequest=new JsonObjectRequest(Request.Method.GET,apiUrl,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ApiHandler","responseJSonArr = "+response.toString());
                        responseJson=response;
                        postSuccRunnable.run();
                        mRequestQueue.stop();

                    }
                },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ApiHandler","error = "+error.toString());
//                Toast.makeText(currentAct,"Error Occured  => "+error.toString(),Toast.LENGTH_SHORT).show();
                mRequestQueue.stop();
                postSuccRunnable.run();
            }
        });

        mRequestQueue.add(jsonRequest);
        mRequestQueue.start();
    }

    public boolean isOnline(){
        ConnectivityManager cm =
                (ConnectivityManager) currentAct.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }
}
