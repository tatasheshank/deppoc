package yantra.com.yantrapoc.shared;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;

/**
 * Created by mypc on 6/10/2018.
 */

public class CommonUi
{
    static Activity currAct;
    private static ProgressDialog loadingDialog;

    public CommonUi(Activity currAct) {
        this.currAct=currAct;
    }

    public static  void showLoading(String message){
        loadingDialog= ProgressDialog.show(currAct, "", message+"...", true, true);//Put this where you need it
        // Set progress dialog style spinner
        loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        // Set the progress dialog background color
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFD4D9D0")));

        loadingDialog.setIndeterminate(false);
        hideLoading();
        loadingDialog.show();
    }
    public static void hideLoading(){
        if(loadingDialog!=null && loadingDialog.isShowing()){
            loadingDialog.dismiss();
        }
    }
    public void showToast(String messsage){
        Toast.makeText(currAct,""+messsage,Toast.LENGTH_SHORT).show();
    }

    public void createDirInStorage(String dirName){
        File root = android.os.Environment.getExternalStorageDirectory();
        File dir = new File (root.getAbsolutePath() +"/"+ dirName); //it is my root directory

        if(!dir.exists()) {
            dir.mkdirs();
        }
    }
    public static double roundDoubleValue(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    public  void enable1autoStart(){
        if(Build.BRAND.equalsIgnoreCase("xiaomi") ){

            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            currAct.startActivity(intent);


        }else if(Build.BRAND.equalsIgnoreCase("Letv")){

            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            currAct.startActivity(intent);

        }
        else if(Build.BRAND.equalsIgnoreCase("Honor")){

            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            currAct.startActivity(intent);

        }




    }
}
