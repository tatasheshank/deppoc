package yantra.com.yantrapoc.services;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import yantra.com.yantrapoc.database.YantraSqliteDatabase;
import yantra.com.yantrapoc.models.DependenciesModel;
import yantra.com.yantrapoc.R;
import yantra.com.yantrapoc.receivers.DesReceiver;
import yantra.com.yantrapoc.shared.ApiHandler;

/**
 * Created by mypc on 6/10/2018.
 */

public class FileMangerService extends Service {
    private YantraSqliteDatabase yantraSqliteDatabase;
    private Intent serviceIntent;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private NotificationManager notificationManager;
    private NotificationCompat.Builder notificationBuilder;


    @Override
    public void onDestroy() {
        super.onDestroy();

        restartService();

    }

    @Override
    public void onStart(final Intent intent, int startId) {
        super.onStart(intent, startId);
        serviceIntent =intent;
        final ApiHandler apiHandler=new ApiHandler(getApplicationContext());
        yantraSqliteDatabase=new YantraSqliteDatabase(this);

        if(apiHandler.isOnline()) {
            apiHandler.fetchFiles(new Runnable() {
                @Override
                public void run() {
                    handleReponseDependencies(apiHandler.responseJson);

                }
            });
        }else{
            stopService(serviceIntent);
        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent){
        super.onTaskRemoved(rootIntent);
        restartService();
    }

    public void restartService(){
        Intent restartServiceIntent = new Intent(getApplicationContext(), DesReceiver.class);
//        restartServiceIntent.setPackage(getPackageName());
//        restartServiceIntent.setAction("DES_SERVICE_STATE_CHANGED");

//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction(getPackageName() + "DES_SERVICE_STATE_CHANGED");
//        intentFilter.addAction(getPackageName() + "android.net.conn.CONNECTIVITY_CHANGE");
//        intentFilter.addAction(getPackageName() + "android.net.wifi.WIFI_STATE_CHANGED");
//        registerReceiver(new DesReceiver(),intentFilter);
        PendingIntent restartServicePendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 5000,
                restartServicePendingIntent);

    }

    public void handleReponseDependencies(JSONObject depsResJsonObj){

        try {
            JSONArray depsJSonArr=depsResJsonObj.getJSONArray("dependencies");
            DependenciesModel[] dependenciesModelsArr=new DependenciesModel[depsJSonArr.length()];
            for (int i=0;i<depsJSonArr.length();i++){
               DependenciesModel dependenciesModel=new DependenciesModel();
                dependenciesModel.convertDepJsonToObject(depsJSonArr.getJSONObject(i));
                dependenciesModelsArr[i]=dependenciesModel;
                yantraSqliteDatabase.insertOrUpdateDependency(dependenciesModel);
            }

            new DownloadFileFromURL().execute(dependenciesModelsArr);

        } catch (JSONException e) {

            stopService(serviceIntent);
        }

    }


    /**
     * Background Async Task to download file
     * */
    class DownloadFileFromURL extends AsyncTask<DependenciesModel, Integer, String> {

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(DependenciesModel... depsModelsArr) {
            int count;
            Bitmap logoBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.yantralogo);

                notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                for (int i = 0; i < depsModelsArr.length; i++) {
                    try {

                        if(depsModelsArr[i].getType().equals("HTML")){
                            continue;
                        }
                        String filePath = depsModelsArr[i].getLocalPath();

                        File outputFile = new File(filePath);
                        long outputFileLength=outputFile.length();
                        // Output stream to write file
                        if(outputFile.exists()){
                            if(Long.parseLong(depsModelsArr[i].getSize())==(outputFileLength)){
                                continue;
                            }
                            else{
                                outputFile.delete();
                            }
                        }

                        URL url = new URL(depsModelsArr[i].getCdnPath());
                    URLConnection conection = url.openConnection();
                    conection.connect();

                    // getting file length
                    long lenghtOfFile = conection.getContentLength();

                    // input stream to read file - with 8k buffer
                    InputStream input = new BufferedInputStream(
                            url.openStream(), 8192);

                        notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                                .setLargeIcon(logoBitmap)

                                .setSmallIcon(R.drawable.yantralogo)
                                .setContentTitle(depsModelsArr[i].getName())
                                .setContentText("Downloading File")
                                .setAutoCancel(true);
                        notificationManager.notify(i, notificationBuilder.build());

                    FileOutputStream output = new FileOutputStream(outputFile);

                    byte data[] = new byte[1024];

                    long total = 0;

                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
                        publishProgress((int) ((total * 100)/lenghtOfFile));
                        Log.e("FileMangerService","length of file "+lenghtOfFile+" , current length "+(int) ((total * 100)/lenghtOfFile));
                        notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                                .setLargeIcon(logoBitmap)

                                .setSmallIcon(R.drawable.yantralogo)
                                .setContentTitle(depsModelsArr[i].getName())
                                .setContentText((int) ((total * 100)/lenghtOfFile)+"%")
                                .setProgress(100,(int) ((total * 100)/lenghtOfFile),false)
                                .setAutoCancel(true);

                        notificationManager.notify(i, notificationBuilder.build());

                        // writing data to file
                        output.write(data, 0, count);
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();
                    notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                            .setLargeIcon(logoBitmap)
                            .setSmallIcon(R.drawable.yantralogo)
                            .setContentTitle(depsModelsArr[i].getName())
                            .setContentText("Completed download")
                            .setAutoCancel(true);

                    notificationManager.notify(i, notificationBuilder.build());
                    } catch (Exception e) {
                        Log.e("Error: ", e.getMessage());
                        notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                                .setLargeIcon(logoBitmap)
                               .setSmallIcon(R.drawable.yantralogo)
                                .setContentTitle(depsModelsArr[i].getName())
                                .setContentText("Error in downloading file.")
                                .setAutoCancel(true);
                        notificationManager.notify(i, notificationBuilder.build());
                        stopService(serviceIntent);
                    }



                }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            stopService(serviceIntent);
        }
    }


}



