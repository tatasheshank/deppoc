package yantra.com.yantrapoc.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mypc on 6/10/2018.
 */

public class DependenciesModel {
    String id,name,type,size,cdnPath,localPath;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getCdnPath() {
        return cdnPath;
    }

    public void setCdnPath(String cdnPath) {
        this.cdnPath = cdnPath;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public void convertDepJsonToObject(JSONObject depsJsonObject){
        try {
            id=depsJsonObject.getString("id")+"";
            name=depsJsonObject.getString("name");
            type=depsJsonObject.getString("type")+"";
            size=depsJsonObject.getString("sizeInBytes")+"";
            cdnPath=depsJsonObject.getString("cdn_path")+"";
            localPath=android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
            if(getType().equals("VIDEO")){
                localPath=localPath+"/YantraDepends/Videos/"+getName();
            }
            else  if(getType().equals("IMAGE")){
                localPath=localPath+"/YantraDepends/Images/"+getName();
            }else  if(getType().equals("HTML")){
                localPath=localPath+"/YantraDepends/HTML/"+getName();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
