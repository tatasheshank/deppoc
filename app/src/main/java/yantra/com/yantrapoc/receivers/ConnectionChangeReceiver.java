package yantra.com.yantrapoc.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import yantra.com.yantrapoc.services.FileMangerService;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by mypc on 6/13/2018.
 */

public class ConnectionChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            try {
//                Toast.makeText(context, "Network is connected", Toast.LENGTH_LONG).show();
                SharedPreferences sharedPreference = context.getSharedPreferences("ServiceSp", MODE_PRIVATE);
                if (sharedPreference.getBoolean("isStartClicked", false)) {
                    Intent fileManServiceIntent = new Intent(context, FileMangerService.class);
                    context.startService(fileManServiceIntent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
