package yantra.com.yantrapoc.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import yantra.com.yantrapoc.services.FileMangerService;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by mypc on 6/10/2018.
 */

public class DesReceiver extends BroadcastReceiver{

        final String  CONN_CHANGE_ACTION="android.net.conn.CONNECTIVITY_CHANGE";
        final String  WIFI_STATE_CHANGED_ACTION= "android.net.wifi.WIFI_STATE_CHANGED";
        final String DES_SERVICE_STATE_CHANGED_ACTION= "DES_SERVICE_STATE_CHANGED";
    @Override
    public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreference = context.getSharedPreferences("ServiceSp", MODE_PRIVATE);
                if (sharedPreference.getBoolean("isStartClicked", false)) {
                    Intent fileManServiceIntent = new Intent(context, FileMangerService.class);
                    context.startService(fileManServiceIntent);
                }
    }
}
