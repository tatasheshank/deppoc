package yantra.com.yantrapoc.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import yantra.com.yantrapoc.models.DependenciesModel;

/**
 * Created by mypc on 6/10/2018.
 */

public class YantraSqliteDatabase extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "pocdepsdb";
    private static final String DEPS_TABLE = "depstable";
    private SQLiteDatabase sqLiteDatabase;
    private final  String TAG="YantraSqliteDatabase";
    public YantraSqliteDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//        sqLiteDatabase = SQLiteDatabase.openOrCreateDatabase("pocdepsdb.db",null);
        this.sqLiteDatabase=getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        Log.e("SQLiteDatabase","onCreate1 ");
        String createFilesTableQuery = "CREATE TABLE If not exists " + DEPS_TABLE + "("
                +   "id TEXT,"
                +   "name TEXT,"
                +   "type TEXT,"
                +   "size TEXT,"
                +   "cdn_path TEXT,"
                +   "local_path TEXT"
                +
                ")";
        sqLiteDatabase.execSQL(createFilesTableQuery);
        Log.e(TAG,"onCreate - DEPS Table created success fully");
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public int insertOrUpdateDependency(DependenciesModel dependenciesModel){
        ContentValues values = new ContentValues();
        values.put("id", dependenciesModel.getId());
        values.put("name", dependenciesModel.getName());
        values.put("type", dependenciesModel.getType());
        values.put("size", dependenciesModel.getSize());
        values.put("cdn_path", dependenciesModel.getCdnPath());
        values.put("local_path", dependenciesModel.getLocalPath());
        try {
            int u = sqLiteDatabase.update(DEPS_TABLE, values, "id=?", new String[]{dependenciesModel.getId()});
            if (u == 0) {

                sqLiteDatabase.insertWithOnConflict(DEPS_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
//                sqLiteDatabase.insert(DEPS_TABLE, null, values);
                Log.e(TAG, "insertOrUpdateDependency - DEPS data inserted successfully");

            } else {
                Log.e(TAG, "insertOrUpdateDependency - DEPS data updated successfully");
            }

            return u;
        }
        catch (Exception e){
            Log.e(TAG,"insertOrUpdateDependency exception "+e.toString());
            return -1;
        }


    }

    public ArrayList<DependenciesModel> getDependencies(){
        ArrayList<DependenciesModel>  dependenciesModels=new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + DEPS_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                DependenciesModel dependenciesModel = new DependenciesModel();
                dependenciesModel.setId(cursor.getString(cursor.getColumnIndex("id")));
                dependenciesModel.setName(cursor.getString(cursor.getColumnIndex("name")));
                dependenciesModel.setType(cursor.getString(cursor.getColumnIndex("type")));
                dependenciesModel.setSize(cursor.getString(cursor.getColumnIndex("size")));
                dependenciesModel.setCdnPath(cursor.getString(cursor.getColumnIndex("cdn_path")));
                dependenciesModel.setLocalPath(cursor.getString(cursor.getColumnIndex("local_path")));

                dependenciesModels.add(dependenciesModel);
            } while (cursor.moveToNext());
            Log.e(TAG,"getDependencies - DEPS modelslist=> "+dependenciesModels.size());

        }
        return dependenciesModels;
    }

    public DependenciesModel getDependencyByID(String depId){
        Cursor cursor = sqLiteDatabase.query(DEPS_TABLE, new String[] {"id","name","type","size","cdn_path","local_path"}, "id" + "=?",
                new String[] { depId }, null, null, null, null);


        if (cursor.moveToFirst()) {
            DependenciesModel dependenciesModel = new DependenciesModel();

                dependenciesModel.setId(cursor.getString(cursor.getColumnIndex("id")));
                dependenciesModel.setName(cursor.getString(cursor.getColumnIndex("name")));
                dependenciesModel.setType(cursor.getString(cursor.getColumnIndex("type")));
                dependenciesModel.setSize(cursor.getString(cursor.getColumnIndex("size")));
                dependenciesModel.setCdnPath(cursor.getString(cursor.getColumnIndex("cdn_path")));
                dependenciesModel.setLocalPath(cursor.getString(cursor.getColumnIndex("local_path")));

            Log.e(TAG,"insertOrUpdateDependency - DEPS model=> "+dependenciesModel.toString());
            return dependenciesModel;
        }
        return null;
    }

    public boolean isDepChangeFound(DependenciesModel newdependenciesModel){
            DependenciesModel olddependenciesModel =getDependencyByID(newdependenciesModel.getId());
            if(!olddependenciesModel.getSize().equals(newdependenciesModel.getSize()) ||
                    !olddependenciesModel.getCdnPath().equals(newdependenciesModel.getCdnPath()) ||
                    !olddependenciesModel.getName().equals(newdependenciesModel.getName())){
                return true;
            }
            else{
                return false;
            }
    }

}
